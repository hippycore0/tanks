import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import DebugUI from './DebugUI';
export default () => {
	const { testBattle, inBattle } = useSelector(
		(state: RootState) => state.battle,
	);
	if (inBattle) {
		return null;
	}

	return <>{testBattle && <DebugUI />}</>;
};
