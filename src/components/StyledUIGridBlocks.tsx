import React, { FC } from 'react';
import { styled } from '@mui/material';
import Box from '@mui/material/Box';

const NotPropagationBox: FC = ({ children, ...props }) => {
	const handleOnMouseDown = (e: any) => {
		e.stopPropagation();
	};
	return (
		<Box onMouseDown={handleOnMouseDown.bind(this)} {...props}>
			{children}
		</Box>
	);
};
export const UIContainer = styled(Box)({
	position: 'absolute',
	top: 0,
	right: 0,
	bottom: 0,
	left: 0,
});

export const UIContainerCentered = styled(Box)({
	position: 'absolute',
	top: 0,
	right: 0,
	bottom: 0,
	left: 0,
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover',
});

export const BgUIContainerCentered = styled(UIContainerCentered)({
	overflow: 'hidden',
	backgroundImage:
		'url(https://static.ubex.com/tanks.app/assets/gui/background.png)',
});
export const UILeftTop = styled(NotPropagationBox)({
	position: 'absolute',
	top: 26,
	left: 23,
});

export const UIRightTop = styled(NotPropagationBox)({
	position: 'absolute',
	top: 26,
	right: 23,
	display: 'flex',
});

export const UIRightBottom = styled(NotPropagationBox)({
	position: 'absolute',
	bottom: 26,
	right: 23,
	display: 'flex',
});

export const UILeftBottom = styled(NotPropagationBox)({
	position: 'absolute',
	bottom: 26,
	left: 23,
});

export const UICenterBottom = styled(NotPropagationBox)({
	position: 'absolute',
	bottom: 26,
	left: 0,
	right: 0,
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center',
});

export const BlackPaper = styled('div')(({ theme }) => ({
	backgroundColor: 'rgba(0,0,0,0.5)',
	borderRadius: 10,
	color: '#fff',
	padding: 10,
	[theme.breakpoints.up('xl')]: {
		padding: 15,
	},
}));

export const StyledAppBox = styled('div')({
	boxSizing: 'border-box',
	backgroundColor: '#adb2be',
	border: '2px solid #461415',
	padding: '25px  20px',
	borderRadius: 10,
});

export const StyledFormGroup = styled('div')({
	marginBottom: 20,
	width: '100%',
	'& > label': {
		color: '#461415',
		display: 'block',
		fontWeight: 'bold',
		marginBottom: 10,
		fontSize: 18,
	},
	'& > input': {
		boxSizing: 'border-box',
		width: '100%',
		border: '2px solid #461415',
		borderRadius: 10,
		display: 'block',
		padding: 10,
		fontSize: 18,
	},
});
