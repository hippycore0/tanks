import React from 'react';
import { useSelector } from 'react-redux';
import { styled } from '@mui/material';
import { RootState } from '../store';
import { Player, Tank } from '../models';

import { StyledAppBox, BgUIContainerCentered } from './StyledUIGridBlocks';

const LobbyHeader = styled('div')({
	fontSize: 30,
	color: '#461414',
	fontWeight: 'bold',
	textAlign: 'center',
	textTransform: 'uppercase',
	marginBottom: 30,
	'& > img': {
		verticalAlign: 'bottom',
		marginLeft: 10,
	},
});
const LobbyTableWrapper = styled(StyledAppBox)({
	marginBottom: 30,
	padding: '5px 10px',
});
const LobbyTable = styled('table')({
	color: '#461414',
	fontWeight: 'bold',
	borderCollapse: 'collapse',
	fontSize: 20,
	'& > tbody > tr > td': {
		padding: '5px',
	},
	'& > tbody > tr > td > img': {
		verticalAlign: 'middle',
		marginRight: 10,
	},
	'& .avatar > img': {
		borderRadius: '100%',
		border: '2px solid #461414',
		width: 70,
		height: 70,
	},
	'& .name': {
		minWidth: 170,
	},
});

export default () => {
	const { inLobby } = useSelector((state: RootState) => state.battle);
	const players = useSelector((state: RootState) => state.players.data);

	if (!inLobby || !players) {
		return null;
	}

	return (
		<BgUIContainerCentered>
			<div>
				<LobbyHeader>
					Waiting players{' '}
					<img src="https://static.ubex.com/tanks.app/assets/gui/star_load.png" />
				</LobbyHeader>
				<LobbyTableWrapper>
					<LobbyTable>
						<tbody>
							{players.map((player: Player) => {
								return (
									<tr>
										<td className="avatar">
											<img src="https://static.ubex.com/tanks.app/assets/gui/card_tank.png" />
										</td>
										<td className="name">
											{player.username}
										</td>
										<td className="tank-icon">
											<img src="https://static.ubex.com/tanks.app/assets/gui/tank_icon.png" />
										</td>
										<td className="tank-icon">
											{player.tanks.map(
												(tank: Tank) =>
													`${tank.name} / `,
											)}
										</td>
									</tr>
								);
							})}
						</tbody>
					</LobbyTable>
				</LobbyTableWrapper>
			</div>
		</BgUIContainerCentered>
	);
};
