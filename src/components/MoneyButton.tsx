import React from 'react';
import { styled } from '@mui/material';
import { AppGradientButton } from './AppGradientButton';

const StyledMoneyButton = styled(AppGradientButton)(({ theme }) => ({
	color: '#fff',
	padding: 0,
	height: 48,
	textShadow: '0px -1px #d35626',
	whiteSpace: 'nowrap',
	[theme.breakpoints.up('lg')]: {
		height: 64,
	},
	[theme.breakpoints.up('xl')]: {
		height: 86,
	},
}));

const MoneyButtonWrapper = styled('div')({
	display: 'flex',
	alignItems: 'center',
	height: '100%',
});

const MoneyButton = () => {
	return (
		<StyledMoneyButton color="yellow">
			<MoneyButtonWrapper>
				<img src="https://static.ubex.com/tanks.app/assets/gui/money.png" />
				&nbsp;100 500&nbsp;
				<img src="https://static.ubex.com/tanks.app/assets/gui/plus.png" />
			</MoneyButtonWrapper>
		</StyledMoneyButton>
	);
};

export default MoneyButton;
