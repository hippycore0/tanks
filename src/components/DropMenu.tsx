import React from 'react';
import { styled } from '@mui/material';
import { StyledSquareButton } from './AppGradientButton';

const DropMenuContainer = styled('div')({
	position: 'relative',
	marginLeft: 15,
});

const DropMenuEntryNumber = styled('div')(({ theme }) => ({
	color: '#fff',
	fontSize: 14,
	border: '1px solid #461415',
	width: 20,
	height: 20,
	backgroundColor: '#d51e23',
	position: 'absolute',
	top: '5px',
	right: '-10px',
	borderRadius: 100,
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	[theme.breakpoints.up('lg')]: {
		fontSize: 16,
		width: 27,
		height: 27,
	},
}));

const DropMenuExpanded = styled('div')(({ theme }) => ({
	position: 'absolute',
	zIndex: 5,
	bottom: 5,
	backgroundColor: '#aeb2be',
	border: '2px solid',
	width: 44,
	paddingBottom: 44,
	borderColor: '#461415',
	borderRadius: '10px',
	'& > .hr': {
		height: '3px',
		borderRadius: '10px',
		backgroundColor: '#7c8089',
	},
	[theme.breakpoints.up('lg')]: {
		paddingBottom: 60,
		width: 60,
	},
	[theme.breakpoints.up('xl')]: {
		paddingBottom: 80,
		width: 82,
	},
}));

const DropMenuExpandedEntry = styled('div')({
	position: 'relative',
	'& > img': {
		maxWidth: '100%',
	},
});

type DropMenuDataEntry = {
	alias: string;
	number: number;
	icon: string;
};

type DropMenuProps = {
	btnData: DropMenuDataEntry;
	entries: DropMenuDataEntry[];
	enable?: boolean;
};

const DropMenu: React.FC<DropMenuProps> = ({
	btnData,
	entries,
	enable = true,
}) => {
	const [isOpen, setIsOpen] = React.useState(false);

	if (!entries || !entries.length) {
		return null;
	}
	return (
		<DropMenuContainer>
			<StyledSquareButton
				onClick={() => setIsOpen(!isOpen)}
				style={{ opacity: enable ? 1 : 0.5, zIndex: 10 }}
			>
				<img src={btnData.icon} alt={btnData.alias} />
				{btnData.number > 0 && (
					<DropMenuEntryNumber>{btnData.number}</DropMenuEntryNumber>
				)}
			</StyledSquareButton>
			{enable && isOpen && entries.length > 1 && (
				<DropMenuExpanded>
					{entries.map((entry, index) => (
						<DropMenuExpandedEntry key={entry.alias}>
							<img src={entry.icon} alt={entry.alias} />
							{index === entries.length - 1 && (
								<div className="hr" />
							)}
							{entry.number > 0 && (
								<DropMenuEntryNumber>
									{entry.number}
								</DropMenuEntryNumber>
							)}
						</DropMenuExpandedEntry>
					))}
				</DropMenuExpanded>
			)}
		</DropMenuContainer>
	);
};

export default DropMenu;
