import React from 'react';
import { styled } from '@mui/material';
import { BlackPaper } from './StyledUIGridBlocks';
import { clearTestPlayers, createTestPlayers } from '../models/Players';
import { setUsername } from '../models/User';
import { startTestBattle, stopTestBattle } from '../models/Battle';
import { testPlayers } from '../models/TestPlayersMock';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { AppGradientButton } from './AppGradientButton';

// @ts-ignore
const staticUrl = STATIC_URL;

const GameModeWrapper = styled(BlackPaper)(({ theme }) => ({
	fontWeight: 'bold',
	fontSize: 32,
	textTransform: 'uppercase',
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	boxSizing: 'border-box',
	backgroundColor: 'rgba(0,0,0,0)',
	borderRadius: 10,
	color: '#fff',
	padding: 15,
	height: 48,
	[theme.breakpoints.up('lg')]: {
		height: 64,
		backgroundColor: 'rgba(0,0,0,0.3)',
	},
	[theme.breakpoints.up('xl')]: {
		height: 86,
	},
}));

const GameModeText = styled('span')(({ theme }) => ({
	display: 'none',
	marginRight: 10,
	[theme.breakpoints.up('xl')]: {
		display: 'inline',
	},
}));
const GameModeSelector = () => {
	const dispatch = useDispatch();
	const { testBattle } = useSelector((state: RootState) => state.battle);
	const handleToggleBattle = () => {
		if (testBattle) {
			dispatch(clearTestPlayers({}));
			dispatch(setUsername(null));
			dispatch(stopTestBattle({}));
		} else {
			dispatch(
				createTestPlayers({
					data: testPlayers,
					activePlayer: 'testUser',
					activeTank: 84,
				}),
			);
			dispatch(setUsername('testUser'));
			dispatch(startTestBattle({}));
		}
	};

	return (
		<GameModeWrapper>
			<GameModeText>Game mode</GameModeText>
			<AppGradientButton
				color="yellow"
				onClick={handleToggleBattle.bind(this)}
			>
				<img src={`${staticUrl}/gui/icon_game_mode_star.png`} />
			</AppGradientButton>
			<AppGradientButton color="yellow" style={{ marginLeft: 10 }}>
				<img src={`${staticUrl}/gui/icon_game_mode_2vs2.png`} />
			</AppGradientButton>
		</GameModeWrapper>
	);
};

export default GameModeSelector;
