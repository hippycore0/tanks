import React from 'react';
import { StyledSquareButton } from './AppGradientButton';
import { Tank } from '../models';
// @ts-ignore
const staticUrl = STATIC_URL;

type ComponentProps = {
	containerProps?: any;
};

const Options: React.FC<ComponentProps> = ({ containerProps, ...props }) => {
	return (
		<StyledSquareButton
			icon={'gui/button_settings.png'}
			{...containerProps}
		>
			<img src={`${staticUrl}/gui/icon_settings.png`} />
		</StyledSquareButton>
	);
};

export default Options;
