import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { Tank } from '../models';
import Options from './Options';
import GameMenu from './GameMenu';
import TankSelectorEntry from './TankSelectorEntry';
import {
	TankSelectorWrapper,
	TankSelectorHeader,
	TankSelectorFooter,
	ToBattleButton,
	TankPreviewContainer,
} from './TankSelectorStyles';
import MoneyButton from './MoneyButton';
import TankScroller from './TanksScroller';
import GameModeSelector from './GameModeSelector';

const TankSelector = () => {
	const dispatch = useDispatch();

	const { playerTanks } = useSelector((state: RootState) => state.players);
	const { battleTankNumber } = useSelector(
		(state: RootState) => state.battle,
	);
	const [selectedTanks, setSelectedTanks] = React.useState([]);

	React.useEffect(() => {
		const defaultSelected = playerTanks.slice(0, battleTankNumber);
		setSelectedTanks(defaultSelected);
	}, [playerTanks]);

	const handleSelectTank = (tank: Tank) => {
		const isSelected = selectedTanks.find((t: Tank) => t.id === tank.id);
		if (isSelected) {
			setSelectedTanks(
				selectedTanks.filter((t: Tank) => t.id !== tank.id),
			);
		} else if (selectedTanks.length < battleTankNumber) {
			setSelectedTanks([...selectedTanks, tank]);
		}
	};

	const readyToBattle = selectedTanks.length === battleTankNumber;
	const handleSubmitSelectedTanks = () => {
		// todo: при повторной битве эвент не отправляется на сервер
		if (readyToBattle) {
			dispatch({
				type: 'socket',
				payload: { tanks: selectedTanks },
				meta: { event: 'select_tanks' },
			});
		}
	};

	if (!playerTanks.length) {
		return null;
	}

	return (
		<TankSelectorWrapper>
			<TankSelectorHeader>
				<GameMenu />
				<GameModeSelector />
				<div style={{ display: 'flex' }}>
					<MoneyButton />
					<Options containerProps={{ style: { marginLeft: 10 } }} />
				</div>
			</TankSelectorHeader>
			<div>
				<TankPreviewContainer>
					{selectedTanks.map((tank: Tank) => (
						<TankSelectorEntry tank={tank} key={tank.id} />
					))}
				</TankPreviewContainer>
			</div>
			<TankSelectorFooter>
				<TankScroller
					selectedTanks={selectedTanks}
					onSelect={handleSelectTank}
				/>
				<ToBattleButton
					style={{ opacity: readyToBattle ? 1 : 0.5 }}
					color="orange"
					onClick={() => handleSubmitSelectedTanks()}
				>
					To battle!
				</ToBattleButton>
			</TankSelectorFooter>
		</TankSelectorWrapper>
	);
};

export default TankSelector;
