import React from 'react';
import { styled } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { UserSelectors } from '../models/User';
import { StyledAppBox, StyledFormGroup } from './StyledUIGridBlocks';
import { AppGradientButton } from './AppGradientButton';

// @ts-ignore
const staticUrl = STATIC_URL;

const AuthWrapper = styled('div')(({ theme }) => ({
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	'& .logo': {
		maxWidth: '100%',
		display: 'none',

		[theme.breakpoints.up('lg')]: {
			maxWidth: 480,
			display: 'block',
			marginLeft: -70,
			marginRight: 20,
		},
	},
}));

const AuthContainer = styled(StyledAppBox)({
	width: 390,
	maxWidth: '100%',
	display: 'flex',
	flexDirection: 'column',
	justifyContent: 'center',
	alignItems: 'center',
	marginBottom: 15,
});

const AuthHeader = styled('div')({
	fontSize: 30,
	textTransform: 'uppercase',
	textAlign: 'center',
	fontWeight: 'bold',
	color: '#461415',
	marginBottom: 30,
});

const AuthFooter = styled('div')({
	fontSize: 20,
	textTransform: 'uppercase',
	textAlign: 'center',
	fontWeight: 'bold',
	color: '#fff',
});

const AuthButton = styled(AppGradientButton)(({ theme }) => ({
	width: 234,
	maxWidth: '100%',
	marginTop: 10,
	marginBottom: 20,
	color: '#461415',
	padding: 5,
	fontSize: 24,
	[theme.breakpoints.up('xl')]: {
		padding: 10,
	},
}));

const SocialsContainer = styled('div')({
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	'& > a': {
		display: 'block',
		margin: '0 15px',
	},
	'& > a > img': {
		width: 60,
	},
});

export default () => {
	const dispatch = useDispatch();
	const handleSubmit = (e: Event) => {
		dispatch({
			type: 'api/auth',
			payload: {
				// @ts-ignore
				username: e.target[0].value,
				// @ts-ignore
				password: e.target[1].value,
			},
		});
		e.preventDefault();
		return false;
	};

	const token = useSelector(UserSelectors.getToken);
	if (token) {
		return null;
	}
	return (
		<AuthWrapper>
			<img
				src={`${staticUrl}/gui/authorization_logo.png`}
				className="logo"
			/>
			<form onSubmit={handleSubmit.bind(this)}>
				<AuthHeader>Sign in</AuthHeader>
				<AuthContainer>
					<StyledFormGroup>
						<label htmlFor="username">Email</label>
						<input id="username" name="username" />
					</StyledFormGroup>
					<StyledFormGroup>
						<label htmlFor="password">Password</label>
						<input id="password" name="password" type="password" />
					</StyledFormGroup>
					<AuthButton color="yellow" type="submit">
						Enter
					</AuthButton>
					<SocialsContainer>
						<a href="#">
							<img src="https://static.ubex.com/tanks.app/assets/gui/authorization_facebook.png" />
						</a>
						<a href="#">
							<img src="https://static.ubex.com/tanks.app/assets/gui/authorization_google.png" />
						</a>
						<a href="#">
							<img src="https://static.ubex.com/tanks.app/assets/gui/authorization_metamask.png" />
						</a>
					</SocialsContainer>
				</AuthContainer>
				<AuthFooter>
					<a href="#">Sign up</a>
				</AuthFooter>
			</form>
		</AuthWrapper>
	);
};
