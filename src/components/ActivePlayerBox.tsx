// https://favpng.com/png_view/soldier-of-no-choice-q-version-avatar-blog-u0e01u0e32u0e23u0e4cu0e15u0e39u0e19u0e0du0e35u0e48u0e1bu0e38u0e48u0e19-cartoon-png/hRXbs0Aa
import React from 'react';
import { useSelector } from 'react-redux';
import { styled } from '@mui/material/styles';
import LinearProgress, {
	linearProgressClasses,
} from '@mui/material/LinearProgress';
import { RootState } from '../store';
import { getStringColorFromString } from '../tools/colors';
import { PlayersSelectors } from '../models/Players';
import { Tank } from '../models';

// @ts-ignore
const staticUrl = STATIC_URL;
// @ts-ignore
const appUrl = APP_URL;

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
	height: 28,
	width: 128,
	borderRadius: 10,
	border: '2px solid #461415',
	[`&.${linearProgressClasses.colorPrimary}`]: {
		backgroundColor: '#25561f',
	},
	[`& .${linearProgressClasses.bar}`]: {
		borderRadius: 0,
		backgroundColor: '#43933a',
	},
}));

const ExpProgress = styled(LinearProgress)(({ theme }) => ({
	height: 18,
	borderRadius: 10,
	border: '2px solid #461415',
	position: 'absolute',
	bottom: -30,
	left: 0,
	right: 0,
	[`&.${linearProgressClasses.colorPrimary}`]: {
		backgroundColor: '#bf5106',
	},
	[`& .${linearProgressClasses.bar}`]: {
		borderRadius: 0,
		backgroundColor: '#f5bc0a',
	},
	[theme.breakpoints.up('lg')]: {
		height: 28,
		bottom: -40,
	},
}));

const ActivePlayerBoxContainer = styled('div')({
	color: '#fff',
	fontWeight: 'bold',
	fontSize: 18,
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
});

const PlayerTanksList = styled('div')({
	display: 'flex',
	marginTop: 10,
	'& > div': {
		marginRight: 25,
		'&:last-child': {
			marginRight: 0,
		},
	},
});

const PlayerTank = styled('div')(({theme}) =>({
	position: 'relative',
	backgroundSize: 'cover',
	backgroundPosition: 'center',
	borderRadius: 12,
	border: '2px solid #461415',
	width: 60,
	height: 60,
	[theme.breakpoints.up('lg')]: {
		width: 90,
		height: 90,
	},
}));

const PlayerTankLvl = styled('div')(({ theme }) => ({
	background: `url(${staticUrl}/gui/level_tank_star.png) center / cover no-repeat`,
	width: 30,
	height: 29,
	color: '#d61e20',
	position: 'absolute',
	top: '-4px',
	right: '-20px',
	fontWeight: 'bold',
	fontSize: 14,
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	[theme.breakpoints.up('lg')]: {
		width: 50,
		height: 48,
		fontSize: 18,
	},
}));

const StaminaNumber = styled('div')({
	background: `url(${staticUrl}/gui/user_statusbar_icon.png) no-repeat center`,
	color: '#fff',
	position: 'absolute',
	left: -32,
	top: -8,
	width: 43,
	height: 47,
	fontWeight: 'bold',
	fontSize: 18,
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
});

const ActivePlayerBox = () => {
	const activeTanks = useSelector(PlayersSelectors.getActivePlayerTanks);
	const { activePlayer, activeTank, stamina } = useSelector(
		(state: RootState) => state.players,
	);
	if (!activePlayer) {
		return null;
	}
	return (
		<>
			<ActivePlayerBoxContainer>
				<div>
					Ходит:&nbsp;
					<span
						style={{
							color: getStringColorFromString(activePlayer),
						}}
					>
						{activePlayer}
					</span>
				</div>
				{stamina[activePlayer] &&
					typeof stamina[activePlayer][activeTank] !==
						'undefined' && (
						<div style={{ position: 'relative' }}>
							<BorderLinearProgress
								variant="determinate"
								value={stamina[activePlayer][activeTank] * 10}
							/>
							<StaminaNumber>
								{stamina[activePlayer][activeTank]}
							</StaminaNumber>
						</div>
					)}
			</ActivePlayerBoxContainer>
			<PlayerTanksList>
				{activeTanks.map((tank: Tank) => (
					<PlayerTank
						key={tank.id}
						style={{
							backgroundImage: `url(${appUrl}${tank.image})`,
						}}
					>
						<PlayerTankLvl>1</PlayerTankLvl>
						<ExpProgress variant="determinate" value={70} />
					</PlayerTank>
				))}
			</PlayerTanksList>
		</>
	);
};
export default ActivePlayerBox;
