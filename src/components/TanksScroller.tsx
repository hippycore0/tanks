import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { RootState } from '../store';
import { Tank } from '../models';
import {
	ScrollerArrowLeft,
	ScrollerArrowRight,
	TanksScrollerContainer,
	TanksScrollerEntry,
	TanksScrollerWrapper,
} from './TankSelectorStyles';
import { Navigation } from 'swiper';
// @ts-ignore
const appUrl = APP_URL;

type TankScrollerProps = {
	onSelect: (tank: Tank) => void;
	selectedTanks: Tank[];
};

const TankScroller: React.FC<TankScrollerProps> = ({
	onSelect,
	selectedTanks,
}) => {
	const { playerTanks } = useSelector((state: RootState) => state.players);
	const [prevEl, setPrevEl] = useState<HTMLElement | null>(null);
	const [nextEl, setNextEl] = useState<HTMLElement | null>(null);

	return (
		<TanksScrollerWrapper>
			<TanksScrollerContainer>
				<Swiper
					navigation={{ prevEl, nextEl }}
					modules={[Navigation]}
					slidesPerView={4}
				>
					{playerTanks.map((tank: Tank) => {
						const isSelected = selectedTanks.find(
							(t: Tank) => t.id === tank.id,
						);
						return (
							<SwiperSlide
								key={tank.id}
								onClick={() => onSelect(tank)}
							>
								<TanksScrollerEntry
									style={{
										backgroundImage: `url(${appUrl}${tank.image})`,
										borderColor: isSelected
											? 'red'
											: '#461415',
									}}
								/>
							</SwiperSlide>
						);
					})}
				</Swiper>
				<ScrollerArrowLeft ref={(node) => setPrevEl(node)} />
				<ScrollerArrowRight ref={(node) => setNextEl(node)} />
			</TanksScrollerContainer>
		</TanksScrollerWrapper>
	);
};

export default TankScroller;
