import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { StyledSquareButton } from './AppGradientButton';

type ComponentProps = {
	containerProps?: any;
};

const NextTurnBtn: React.FC<ComponentProps> = ({ containerProps }) => {
	const { activePlayer, activeTank, hp } = useSelector(
		(state: RootState) => state.players,
	);
	const { username } = useSelector((state: RootState) => state.user);
	const dispatch = useDispatch();
	React.useEffect(() => {
		if (hp[activePlayer] && hp[activePlayer][activeTank] === 0) {
			dispatch({
				type: 'socket',
				payload: {
					username,
				},
				meta: { event: 'turn' },
			});
		}
	}, [activePlayer]);
	const handleClick = (e: React.MouseEvent) => {
		e.preventDefault();
		dispatch({
			type: 'socket',
			payload: {
				username,
			},
			meta: { event: 'turn' },
		});
		return false;
	};

	return (
		<StyledSquareButton
			color="orange"
			onClick={(e: React.MouseEvent<HTMLElement>) => handleClick(e)}
			{...containerProps}
		>
			<img
				src="https://static.ubex.com/tanks.app/assets/gui/no_step.png"
				alt="Next turn"
			/>
		</StyledSquareButton>
	);
};

export default NextTurnBtn;
