import { styled } from '@mui/material';
import React from 'react';

interface AppGradientButtonProps {
	color?: 'green' | 'yellow' | 'orange' | 'gray';
	[x: string]: any;
}

export const StyledAppButton = styled('button')<AppGradientButtonProps>(
	({ theme, color }) => {
		const styles = {
			backgroundColor: '#f6bc08',
			color: '#fff',
			textTransform: 'uppercase',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			borderRadius: 12,
			fontSize: 30,
			fontWeight: 'bold',
			border: '2px solid #461415',
			position: 'relative',
			padding: 0,
			'&::before': {
				position: 'absolute',
				top: 3,
				bottom: 6,
				left: 0,
				right: 0,
				display: 'block',
				content: '""',
				borderRadius: 12,
			},
			'& > .AppGradientButtonValue': {
				position: 'relative',
				zIndex: 1,
				height: '100%',
				width: '100%',
				boxSizing: 'border-box',
				padding: '4px 10px 6px 10px',
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
			},
			'& img': {
				maxWidth: '100%',
				maxHeight: 'calc(100% - 9px)',
			},
		};
		return modifyBgColor(styles, color);
	},
);

const modifyBgColor = (
	styles: any,
	color: 'green' | 'yellow' | 'orange' | 'gray',
) => {
	const colors = {
		green: {
			bl: '#044201',
			tl: '#00961b',
		},
		yellow: {
			bl: 'linear-gradient(0deg, #bf5105 50%, #fdd43b 50%)',
			tl: '#f6bc08',
		},
		orange: {
			bl: 'linear-gradient(0deg, #7c2200 50%, #e48028 50%)',
			tl: '#d35500',
		},
		gray: {
			bl: 'linear-gradient(0deg, #657784 50%, #f7fdf6 50%)',
			tl: '#d8dfed',
		},
	};
	return {
		...styles,
		background: colors[color].bl,
		'&::before': {
			...styles['&::before'],
			background: colors[color].tl,
		},
	};
};

export const AppGradientButton: React.FC<AppGradientButtonProps> = ({
	color = 'gray',
	children,
	...props
}) => {
	return (
		<StyledAppButton color={color} {...props}>
			<div className="AppGradientButtonValue">{children}</div>
		</StyledAppButton>
	);
};

interface StyledSquareButtonProps {}

export const StyledSquareButton = styled(AppGradientButton)<
	StyledSquareButtonProps
>(({ theme, ...props }) => {
	return {
		width: 48,
		height: 48,
		'& > .AppGradientButtonValue': {
			padding: '4px 0px 6px 0px',
		},
		[theme.breakpoints.up('lg')]: {
			width: 64,
			height: 64,
		},
		[theme.breakpoints.up('xl')]: {
			width: 86,
			height: 86,
		},
	};
});
