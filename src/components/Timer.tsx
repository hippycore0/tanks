import React from 'react';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { styled } from '@mui/material';
import { RootState } from '../store';
import { AppGradientButton } from './AppGradientButton';

const TimerContainer = styled(AppGradientButton)(({ theme }) => ({
	fontWeight: 'bold',
	fontSize: 30,
	textAlign: 'center',
	color: '#000',
	[theme.breakpoints.up('lg')]: {
		fontSize: 38,
	},
}));

export default () => {
	const { duration } = useSelector((state: RootState) => state.battle);
	const formatted = moment().startOf('day').seconds(duration).format('mm:ss');
	return <TimerContainer color="yellow">{formatted}</TimerContainer>;
};
