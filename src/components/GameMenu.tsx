import { StyledSquareButton } from './AppGradientButton';
import React from 'react';
import { styled } from '@mui/material';

// @ts-ignore
const staticUrl = STATIC_URL;
const Wrapper = styled('div')({
	display: 'flex',
	'& > a, & > button, & > div': {
		marginLeft: 10,
	},
	'& > a:first-child, & > button:first-child, & > div:first-child': {
		marginLeft: 0,
	},
});

const GameMenu = () => {
	return (
		<Wrapper>
			<StyledSquareButton>
				<img src={`${staticUrl}/gui/icon_home.png`} />
			</StyledSquareButton>
			<StyledSquareButton>
				<img src={`${staticUrl}/gui/icon_shop.png`} />
			</StyledSquareButton>
			<StyledSquareButton>
				<img src={`${staticUrl}/gui/icon_warehouse.png`} />
			</StyledSquareButton>
			<StyledSquareButton>
				<img src={`${staticUrl}/gui/icon_friends.png`} />
			</StyledSquareButton>
		</Wrapper>
	);
};

export default GameMenu;
