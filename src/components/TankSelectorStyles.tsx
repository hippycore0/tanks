import { styled } from '@mui/material';
import { StyledAppBox } from './StyledUIGridBlocks';
import { AppGradientButton } from './AppGradientButton';

// @ts-ignore
const staticUrl = STATIC_URL;

export const TankSelectorWrapper = styled('div')({
	display: 'flex',
	width: '100%',
	height: '100%',
	justifyContent: 'space-between',
	flexDirection: 'column',
});
export const TankSelectorHeader = styled('div')({
	display: 'flex',
	justifyContent: 'space-between',
	margin: 25,
	marginBottom: 0,
});

export const TankSelectorFooter = styled('div')({
	display: 'flex',
	margin: 25,
	marginBottom: 55,
	justifyContent: 'center',
	alignItems: 'center',
});
export const ToBattleButton = styled(AppGradientButton)({
	fontSize: 22,
	padding: '20px 25px',
	marginLeft: 'auto',
	transform: 'translateX(-40%)',
});
export const TanksScrollerWrapper = styled('div')({
	position: 'absolute',
	left: '50%',
	transform: 'translateX(-50%)',
});
export const TanksScrollerContainer = styled(StyledAppBox)(({ theme }) => ({
	margin: '0 15px',
	padding: 10,
	width: 300,
}));
export const ScrollerArrows = styled('div')({
	position: 'absolute',
	width: 25,
	height: 40,
	top: '50%',
	marginTop: -20,
});
export const ScrollerArrowLeft = styled(ScrollerArrows)({
	left: -20,
	backgroundImage: `url(${staticUrl}/gui/down_panel_scroll_left.png)`,
});
export const ScrollerArrowRight = styled(ScrollerArrows)({
	right: -20,
	backgroundImage: `url(${staticUrl}/gui/down_panel_scroll_right.png)`,
});
export const TanksScrollerEntry = styled('div')(({ theme }) => ({
	width: 60,
	height: 60,
	border: '2px solid #461415',
	borderRadius: 10,
	backgroundSize: 'cover',
	backgroundPosition: 'center',
	marginLeft: 10,
	'&:first-child': {
		marginLeft: 0,
	},
}));
export const TankPreviewContainer = styled('div')({
	display: 'flex',
});
