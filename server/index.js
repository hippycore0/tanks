const express = require('express');
const cors = require('cors');
const { createServer } = require('http');
const { Server } = require('socket.io');
const players = {};
const app = express();
app.use(cors());
app.options('*', cors());
const httpServer = createServer(app);
const io = new Server(httpServer, {
	/* options */
	cors: { origin: '*' },
});
// https://habr.com/ru/post/544046/
// https://gamedevacademy.org/create-a-basic-multiplayer-game-in-phaser-3-with-socket-io-part-1/
app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});
httpServer.listen(8081, function () {
	console.log(`Listening on ${httpServer.address().port}`);
});

io.on('connection', function (socket) {
	console.log('a user connected');
	// create a new player and add it to our players object
	players[socket.id] = {
		rotation: 0,
		x: Math.floor(Math.random() * 700) + 50,
		y: Math.floor(Math.random() * 500) + 50,
		playerId: socket.id,
		team: Math.floor(Math.random() * 2) === 0 ? 'red' : 'blue',
	};
	// send the players object to the new player
	socket.emit('currentPlayers', players);
	// update all other players of the new player
	socket.broadcast.emit('newPlayer', players[socket.id]);
	// when a player disconnects, remove them from our players object
	socket.on('disconnect', function () {
		console.log('user disconnected');
		// remove this player from our players object
		delete players[socket.id];
		// emit a message to all players to remove this player
		io.emit('user-unjoin', socket.id);
	});
});
